package com.prosor.wedoogiftTest.controller;

import com.prosor.wedoogiftTest.controller.DTO.SendDepositDTO;
import com.prosor.wedoogiftTest.models.Company;
import com.prosor.wedoogiftTest.repositories.CompanyRepository;
import com.prosor.wedoogiftTest.repositories.EmployeeRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class CompanyController {
    private CompanyRepository companyRepository;

    private  EmployeeRepository employeeRepository;

    private static final Logger logger = LogManager.getLogger(CompanyController.class.getName());

    @Autowired
    public CompanyController(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    @PostMapping("/company/create")
    public ResponseEntity<Object> createCompany(@RequestBody Company company){
        logger.info("create company" + company);
        return new ResponseEntity<>(companyRepository.save(company), HttpStatus.OK);
    }

    @GetMapping("/company/{id}")
    public ResponseEntity<Object> getCompanyById(@PathVariable Long id){
        logger.info("get company");
        Optional<Company> company = companyRepository.findById(id);


        if(company.isPresent()){
            return new ResponseEntity<>(company, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Company not found", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("company/delete/{id}")
    public ResponseEntity<Object> deleteCompany(@PathVariable Long id){
        companyRepository.deleteById(id);
        return new ResponseEntity<>("Company removed", HttpStatus.OK);
    }

    @PutMapping("/company/update")
    public ResponseEntity<Object> updateCompany(@RequestBody Company company){
        return new ResponseEntity<>(companyRepository.save(company), HttpStatus.OK);
    }

    @PostMapping("company/sendGiftDeposit")
    public ResponseEntity<Object> sendGiftDeposit(@RequestBody SendDepositDTO sendDepositDTO){

        if(sendDepositDTO.getCompany().sendGiftDeposit(sendDepositDTO.getEmployee(), sendDepositDTO.getAmount())){
            companyRepository.save(sendDepositDTO.getCompany());
            employeeRepository.save(sendDepositDTO.getEmployee());
            return new ResponseEntity<>("Sending Gift Deposit OK", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Sending Gift Deposit not allowed", HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("company/sendMealDeposit")
    public ResponseEntity<Object> sendMealDeposit(@RequestBody SendDepositDTO sendDepositDTO){
        if(sendDepositDTO.getCompany().sendMealDeposit(sendDepositDTO.getEmployee(), sendDepositDTO.getAmount())){
            companyRepository.save(sendDepositDTO.getCompany());
            employeeRepository.save(sendDepositDTO.getEmployee());
            return new ResponseEntity<>("Sending Gift Deposit OK", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Sending Gift Deposit not allowed", HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/companies")
    public Iterable<Company> getCompanies(){
        return companyRepository.findAll();
    }

}
