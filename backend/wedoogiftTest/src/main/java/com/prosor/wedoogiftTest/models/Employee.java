package com.prosor.wedoogiftTest.models;

import javax.persistence.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<GiftDeposit> giftDeposits = new HashSet<>();

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<MealDeposit> mealDeposits = new HashSet<>();;

    public Employee() {
    }

    public Employee(Long id, String firstName, String lastName, Set<GiftDeposit> giftDeposits, Set<MealDeposit> mealDeposits) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.giftDeposits = giftDeposits;
        this.mealDeposits = mealDeposits;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<GiftDeposit> getGiftDeposits() {
        return giftDeposits;
    }

    public void setGiftDeposits(Set<GiftDeposit> giftDeposits) {
        this.giftDeposits = giftDeposits;
    }

    public Set<MealDeposit> getMealDeposits() {
        return mealDeposits;
    }

    public void setMealDeposits(Set<MealDeposit> mealDeposits) {
        this.mealDeposits = mealDeposits;
    }

    public void addGiftDeposit(long amount, Company company) {
        this.giftDeposits.add(new GiftDeposit(amount, company.getId()));
    }

    public void addMealDeposit(long amount, Company company) {
        this.mealDeposits.add(new MealDeposit(amount, company.getId()));
    }

    public HashMap<String, Long> getBalance() {
        HashMap<String, Long> res = new HashMap<>();

        Long mealDepositAmount = mealDeposits.stream()
                .filter(MealDeposit::isValid)
                .mapToLong(MealDeposit::getAmount)
                .reduce(0L, Long::sum);

        Long giftDepositAmount = giftDeposits.stream()
                .filter(GiftDeposit::isValid)
                .mapToLong(GiftDeposit::getAmount)
                .reduce(0L, Long::sum);


        res.put("giftDepositAmount", giftDepositAmount);
        res.put("mealDepositAmount", mealDepositAmount);

        return res;

    }
}
