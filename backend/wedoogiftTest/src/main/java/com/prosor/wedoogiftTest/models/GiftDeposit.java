package com.prosor.wedoogiftTest.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Entity
public class GiftDeposit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long amount;

    private LocalDate emittingDate;


    private Long emittingCompanyId;

    public GiftDeposit() {
    }

    public GiftDeposit(Long id, Long amount, LocalDate emittingDate, Long emittingCompanyId) {
        this.id = id;
        this.amount = amount;
        this.emittingDate = emittingDate;
        this.emittingCompanyId = emittingCompanyId;
    }

    public GiftDeposit(long amount, Long companyId) {
        this.amount = amount;
        this.emittingCompanyId = companyId;
        emittingDate = LocalDate.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public LocalDate getEmittingDate() {
        return emittingDate;
    }

    public void setEmittingDate(LocalDate emittingDate) {
        this.emittingDate = emittingDate;
    }

    public Long getEmittingCompanyId() {
        return emittingCompanyId;
    }

    public void setEmittingCompany(Long emittingCompanyId) {
        this.emittingCompanyId = emittingCompanyId;
    }

    public boolean isValid() {
        return LocalDate.now().isBefore(emittingDate.plusDays(365));
    }
}
