package com.prosor.wedoogiftTest.models.builder;

import com.prosor.wedoogiftTest.models.Company;
import com.prosor.wedoogiftTest.models.MealDeposit;

import java.time.LocalDate;
import java.util.Date;

public final class MealDepositBuilder {
    private Long id;
    private Long amount;
    private LocalDate emittingDate;
    private Company emittingCompany;

    private MealDepositBuilder() {
    }

    public static MealDepositBuilder aMealDeposit() {
        return new MealDepositBuilder();
    }

    public MealDepositBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public MealDepositBuilder withAmount(Long amount) {
        this.amount = amount;
        return this;
    }

    public MealDepositBuilder withEmittingDate(LocalDate emittingDate) {
        this.emittingDate = emittingDate;
        return this;
    }

    public MealDepositBuilder withEmittingCompany(Company emittingCompany) {
        this.emittingCompany = emittingCompany;
        return this;
    }

    public MealDeposit build() {
        MealDeposit mealDeposit = new MealDeposit();
        mealDeposit.setId(id);
        mealDeposit.setAmount(amount);
        mealDeposit.setEmittingDate(emittingDate);
        mealDeposit.setEmittingCompany(emittingCompany);
        return mealDeposit;
    }
}
