package com.prosor.wedoogiftTest.models.builder;

import com.prosor.wedoogiftTest.models.Employee;
import com.prosor.wedoogiftTest.models.GiftDeposit;
import com.prosor.wedoogiftTest.models.MealDeposit;

import java.util.HashSet;
import java.util.Set;

public final class EmployeeBuilder {
    private Long id;
    private String firstName;
    private String lastName;
    private Set<GiftDeposit> giftDeposits = new HashSet<>();
    private Set<MealDeposit> mealDeposits = new HashSet<>();

    private EmployeeBuilder() {
    }

    public static EmployeeBuilder anEmployee() {
        return new EmployeeBuilder();
    }

    public EmployeeBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public EmployeeBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public EmployeeBuilder withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public EmployeeBuilder withGiftDeposits(Set<GiftDeposit> giftDeposits) {
        this.giftDeposits = giftDeposits;
        return this;
    }

    public EmployeeBuilder withMealDeposits(Set<MealDeposit> mealDeposits) {
        this.mealDeposits = mealDeposits;
        return this;
    }



    public Employee build() {
        Employee employee = new Employee();
        employee.setId(id);
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setGiftDeposits(giftDeposits);
        employee.setMealDeposits(mealDeposits);
        return employee;
    }
}
