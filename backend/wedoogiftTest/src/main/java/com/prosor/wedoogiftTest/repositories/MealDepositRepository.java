package com.prosor.wedoogiftTest.repositories;

import com.prosor.wedoogiftTest.models.MealDeposit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MealDepositRepository extends CrudRepository<MealDeposit, Long> {
}
