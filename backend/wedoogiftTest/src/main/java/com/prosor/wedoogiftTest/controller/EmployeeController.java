package com.prosor.wedoogiftTest.controller;

import com.prosor.wedoogiftTest.models.Employee;
import com.prosor.wedoogiftTest.repositories.EmployeeRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class EmployeeController {
    private static final Logger logger = LogManager.getLogger(EmployeeController.class.getName());
    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @PostMapping("/employee/create")
    public ResponseEntity<Object> createEmployee(@RequestBody Employee employee){
        logger.debug("Create Employee");
        return new ResponseEntity<>(employeeRepository.save(employee), HttpStatus.OK);
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<Object> getEmployeeById(@PathVariable Long id){
        logger.debug("Get Employee");
        Optional<Employee> employee = employeeRepository.findById(id);

        if(employee.isPresent()){
            return new ResponseEntity<>(employee, HttpStatus.OK);
        }
        else{
            logger.debug("Employee not found");
            return new ResponseEntity<>("Employee not found", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("employee/delete/{id}")
    public ResponseEntity<Object> deleteEmployee(@PathVariable Long id){
        logger.debug("Delete employee");
        employeeRepository.deleteById(id);
        return new ResponseEntity<>("Employee removed", HttpStatus.OK);
    }

    @PutMapping("employee/update")
    public ResponseEntity<Object> updateEmployee(@RequestBody Employee employee){
        logger.debug("Update Employee");
        return new ResponseEntity<>(employeeRepository.save(employee), HttpStatus.OK);
    }

    @GetMapping("/employees")
    public Iterable<Employee> getEmployees(){
        return employeeRepository.findAll();
    }
}
