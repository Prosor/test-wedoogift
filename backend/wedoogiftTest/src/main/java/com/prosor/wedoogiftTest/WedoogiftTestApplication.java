package com.prosor.wedoogiftTest;

import com.prosor.wedoogiftTest.models.Company;
import com.prosor.wedoogiftTest.models.Employee;
import com.prosor.wedoogiftTest.models.builder.CompanyBuilder;
import com.prosor.wedoogiftTest.models.builder.EmployeeBuilder;
import com.prosor.wedoogiftTest.repositories.CompanyRepository;
import com.prosor.wedoogiftTest.repositories.EmployeeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Optional;

@SpringBootApplication
public class WedoogiftTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(WedoogiftTestApplication.class, args);
	}


	@Bean
	public CommandLineRunner demo(CompanyRepository companyRepository, EmployeeRepository employeeRepository){
		return (args) -> {
			Company company = CompanyBuilder.aCompany().withName("Wedoogift").withBalance(5000L).build();
			Employee employee = EmployeeBuilder.anEmployee().withFirstName("Nicolas").withLastName("MARTIN").build();
			Employee employee1 = EmployeeBuilder.anEmployee().withFirstName("Jean").withLastName("Dupont").build();
			employeeRepository.save(employee);
			employeeRepository.save(employee1);
			companyRepository.save(company);

			company = companyRepository.findById(1L).get();
			employee = employeeRepository.findById(1L).get();
			company.getEmployees().add(employee);

			companyRepository.save(company);





		};
	}
}
