package com.prosor.wedoogiftTest.models.builder;

import com.prosor.wedoogiftTest.models.Company;
import com.prosor.wedoogiftTest.models.Employee;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public final class CompanyBuilder {
    private Long id;
    private String name;
    private Long balance;
    private Set<Employee> employees = new HashSet<Employee>();

    private CompanyBuilder() {
    }

    public static CompanyBuilder aCompany() {
        return new CompanyBuilder();
    }

    public CompanyBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public CompanyBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public CompanyBuilder withBalance(Long balance) {
        this.balance = balance;
        return this;
    }

    public CompanyBuilder withEmployees(Set<Employee> employees) {
        this.employees = employees;
        return this;
    }

    public CompanyBuilder withEmployees(Employee... employees) {
        this.employees.addAll(Arrays.asList(employees));
        return this;
    }

    public Company build() {
        Company company = new Company();
        company.setId(id);
        company.setName(name);
        company.setBalance(balance);
        company.setEmployees(employees);
        return company;
    }
}
