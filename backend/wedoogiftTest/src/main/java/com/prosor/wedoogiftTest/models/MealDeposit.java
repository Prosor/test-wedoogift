package com.prosor.wedoogiftTest.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

@Entity
public class MealDeposit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long amount;

    private LocalDate emittingDate;

    private Long emittingCompanyId;

    public MealDeposit() {
    }

    public MealDeposit(Long id, Long amount, LocalDate emittingDate, Long emittingCompanyId) {
        this.id = id;
        this.amount = amount;
        this.emittingDate = emittingDate;
        this.emittingCompanyId = emittingCompanyId;
    }

    public MealDeposit(long amount, Long companyId) {
        this.amount = amount;
        this.emittingCompanyId = companyId;
        this.emittingDate = LocalDate.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public LocalDate getEmittingDate() {
        return emittingDate;
    }

    public void setEmittingDate(LocalDate emittingDate) {
        this.emittingDate = emittingDate;
    }

    public Long getEmittingCompany() {
        return emittingCompanyId;
    }

    public void setEmittingCompany(Company emittingCompany) {
        this.emittingCompanyId = emittingCompanyId;
    }

    public boolean isValid() {

        LocalDate endValidityDate = LocalDate.of(emittingDate.getYear()+1, 2, 1).with(TemporalAdjusters.lastDayOfMonth());

        return LocalDate.now().isBefore(endValidityDate);
    }
}
