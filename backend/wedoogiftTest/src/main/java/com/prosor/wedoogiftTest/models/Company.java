package com.prosor.wedoogiftTest.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Long balance;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Employee> employees;

    public Company() {
    }

    public Company(Long id, String name, Long balance, Set<Employee> employees) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.employees = employees;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    public boolean sendGiftDeposit(Employee employee, long amount) {
        if(isValidSend(employee, amount)) {
            this.balance -= amount;
            employee.addGiftDeposit(amount, this);
            return true;
        }
        else{
            return false;
        }
    }

    public boolean sendMealDeposit(Employee employee, long amount) {
        if(isValidSend(employee, amount)) {
            this.balance -= amount;
            employee.addMealDeposit(amount, this);
            return true;
        }
        else{
            return false;
        }
    }

    private boolean isValidSend(Employee employee, long amount){
        return (balance > amount) && (this.employees.stream().filter((em) -> em.getId() == employee.getId()) != null);
    }
}
