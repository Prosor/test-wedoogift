package com.prosor.wedoogiftTest.controller.DTO;

import com.prosor.wedoogiftTest.models.Company;
import com.prosor.wedoogiftTest.models.Employee;

public class SendDepositDTO {
    private Employee employee;
    private Company company;
    private Long amount;

    public SendDepositDTO(Employee employee, Company company, Long amount) {
        this.employee = employee;
        this.company = company;
        this.amount = amount;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
