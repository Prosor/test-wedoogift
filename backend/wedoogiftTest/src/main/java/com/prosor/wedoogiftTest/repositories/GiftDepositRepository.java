package com.prosor.wedoogiftTest.repositories;

import com.prosor.wedoogiftTest.models.GiftDeposit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GiftDepositRepository extends CrudRepository<GiftDeposit, Long> {
}
