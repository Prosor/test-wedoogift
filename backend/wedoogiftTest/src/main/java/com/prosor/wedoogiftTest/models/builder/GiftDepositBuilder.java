package com.prosor.wedoogiftTest.models.builder;

import com.prosor.wedoogiftTest.models.Company;
import com.prosor.wedoogiftTest.models.GiftDeposit;

import java.time.LocalDate;
import java.util.Date;

public final class GiftDepositBuilder {
    private Long id;
    private Long amount;
    private LocalDate emittingDate;
    private Long emittingCompanyId;

    private GiftDepositBuilder() {
    }

    public static GiftDepositBuilder aGiftDeposit() {
        return new GiftDepositBuilder();
    }

    public GiftDepositBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public GiftDepositBuilder withAmount(Long amount) {
        this.amount = amount;
        return this;
    }

    public GiftDepositBuilder withEmittingDate(LocalDate emittingDate) {
        this.emittingDate = emittingDate;
        return this;
    }

    public GiftDepositBuilder withEmittingCompany(Long emittingCompanyId) {
        this.emittingCompanyId = emittingCompanyId;
        return this;
    }

    public GiftDeposit build() {
        GiftDeposit giftDeposit = new GiftDeposit();
        giftDeposit.setId(id);
        giftDeposit.setAmount(amount);
        giftDeposit.setEmittingDate(emittingDate);
        giftDeposit.setEmittingCompany(emittingCompanyId);
        return giftDeposit;
    }
}
