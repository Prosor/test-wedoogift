package com.prosor.wedoogiftTest.models;

import com.prosor.wedoogiftTest.models.builder.CompanyBuilder;
import com.prosor.wedoogiftTest.models.builder.EmployeeBuilder;
import com.prosor.wedoogiftTest.models.builder.GiftDepositBuilder;
import com.prosor.wedoogiftTest.models.builder.MealDepositBuilder;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;


class EmployeeTest {

    @Test
    void whenGetUserBalanceWithValidGiftAmountIsTrue(){

        //GIVEN
        Employee employee = EmployeeBuilder.anEmployee().withFirstName("Nicolas").withLastName("MARTIN").build();
        Company company = CompanyBuilder.aCompany().withName("Wedoogift").withBalance(5000L).withEmployees(employee).build();

        company.sendGiftDeposit(employee, 50L);

        Assertions.assertThat(employee.getBalance().get("giftDepositAmount")).isEqualTo(50L);

    }

    @Test
    void whenGetUserBalanceWithInvalidGiftAmountIsTrue(){

        //GIVEN
        Employee employee = EmployeeBuilder.anEmployee().withFirstName("Nicolas").withLastName("MARTIN").build();
        Company company = CompanyBuilder.aCompany().withName("Wedoogift").withBalance(5000L).withEmployees(employee).build();

        company.sendGiftDeposit(employee, 50L);

        employee.getGiftDeposits().add(
                GiftDepositBuilder.aGiftDeposit().withAmount(150L).withEmittingCompany(company.getId()).withEmittingDate(LocalDate.of(2021, 3,1)).build());

        Assertions.assertThat(employee.getBalance().get("giftDepositAmount")).isEqualTo(50L);
    }

    @Test
    void whenGetUserBalanceWithValidMealAmountIsTrue(){

        //GIVEN
        Employee employee = EmployeeBuilder.anEmployee().withFirstName("Nicolas").withLastName("MARTIN").build();
        Company company = CompanyBuilder.aCompany().withName("Wedoogift").withBalance(5000L).withEmployees(employee).build();

        company.sendMealDeposit(employee, 50L);

        Assertions.assertThat(employee.getBalance().get("mealDepositAmount")).isEqualTo(50L);

    }

    @Test
    void whenGetUserBalanceWithInvalidMealAmountIsTrue(){

        //GIVEN
        Employee employee = EmployeeBuilder.anEmployee().withFirstName("Nicolas").withLastName("MARTIN").build();
        Company company = CompanyBuilder.aCompany().withName("Wedoogift").withBalance(5000L).withEmployees(employee).build();

        company.sendMealDeposit(employee, 50L);

        employee.getMealDeposits().add(
                MealDepositBuilder.aMealDeposit().withAmount(150L).withEmittingCompany(company).withEmittingDate(LocalDate.of(2021, 3,1)).build());

        Assertions.assertThat(employee.getBalance().get("mealDepositAmount")).isEqualTo(50L);

    }

}