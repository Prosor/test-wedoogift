package com.prosor.wedoogiftTest.models;

import com.prosor.wedoogiftTest.models.Company;
import com.prosor.wedoogiftTest.models.Employee;
import com.prosor.wedoogiftTest.models.builder.CompanyBuilder;
import com.prosor.wedoogiftTest.models.builder.EmployeeBuilder;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;


class CompanyTest {

    @Test
    void whenCompanyEmitsGiftDepositEmployeeHasTheGiftDeposit() {
        //GIVEN
        Employee employee = EmployeeBuilder.anEmployee().withFirstName("Nicolas").withLastName("MARTIN").build();
        Employee employee2 = EmployeeBuilder.anEmployee().withFirstName("Jean").withLastName("DUPONT").build();
        Company company = CompanyBuilder.aCompany().withName("Wedoogift").withBalance(5000L).withEmployees(employee, employee2).build();


        //WHEN

        boolean result = company.sendGiftDeposit(employee, 50L);

        //THEN

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(company.getBalance()).isEqualTo(4950L);
            softly.assertThat(employee.getGiftDeposits()).isNotEmpty();
            softly.assertThat(result).isTrue();
        });
    }

    @Test
    void whenCompanyEmitsGiftDepositToEmployeeNotInCompanyReturnFalse() {
        //GIVEN
        Employee employee = EmployeeBuilder.anEmployee().withFirstName("Nicolas").withLastName("MARTIN").build();
        Employee employee2 = EmployeeBuilder.anEmployee().withFirstName("Jean").withLastName("DUPONT").build();
        Employee employee3 = EmployeeBuilder.anEmployee().withFirstName("Pierre").withLastName("MANGIN").build();
        Company company = CompanyBuilder.aCompany().withName("Wedoogift").withBalance(5000L).withEmployees(employee, employee2).build();

        //WHEN
        boolean result = company.sendGiftDeposit(employee3, 50L);

        //THEN
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(company.getBalance()).isEqualTo(5000L);
            softly.assertThat(employee3.getGiftDeposits()).isEmpty();
            softly.assertThat(result).isFalse();
        });
    }

    @Test
    void whenCompanyEmitsGiftDepositWithBalanceLowerOfAmountReturnFalse() {
        //GIVEN
        Employee employee = EmployeeBuilder.anEmployee().withFirstName("Nicolas").withLastName("MARTIN").build();
        Employee employee2 = EmployeeBuilder.anEmployee().withFirstName("Jean").withLastName("DUPONT").build();
        Employee employee3 = EmployeeBuilder.anEmployee().withFirstName("Pierre").withLastName("MANGIN").build();
        Company company = CompanyBuilder.aCompany().withName("Wedoogift").withBalance(500L).withEmployees(employee, employee2).build();

        //WHEN
        boolean result = company.sendGiftDeposit(employee3, 1000L);

        //THEN
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result).isFalse();
        });
    }

    @Test
    void whenCompanyEmitsMealDepositEmployeeHasTheGiftDeposit() {
        //GIVEN
        Employee employee = EmployeeBuilder.anEmployee().withFirstName("Nicolas").withLastName("MARTIN").build();
        Employee employee2 = EmployeeBuilder.anEmployee().withFirstName("Jean").withLastName("DUPONT").build();
        Company company = CompanyBuilder.aCompany().withName("Wedoogift").withBalance(5000L).withEmployees(employee, employee2).build();


        //WHEN

        boolean result = company.sendMealDeposit(employee, 50L);

        //THEN

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(company.getBalance()).isEqualTo(4950L);
            softly.assertThat(employee.getMealDeposits()).isNotEmpty();
            softly.assertThat(result).isTrue();
        });
    }

    @Test
    void whenCompanyEmitsMealDepositToEmployeeNotInCompanyReturnFalse() {
        //GIVEN
        Employee employee = EmployeeBuilder.anEmployee().withFirstName("Nicolas").withLastName("MARTIN").build();
        Employee employee2 = EmployeeBuilder.anEmployee().withFirstName("Jean").withLastName("DUPONT").build();
        Employee employee3 = EmployeeBuilder.anEmployee().withFirstName("Pierre").withLastName("MANGIN").build();
        Company company = CompanyBuilder.aCompany().withName("Wedoogift").withBalance(5000L).withEmployees(employee, employee2).build();

        //WHEN
        boolean result = company.sendMealDeposit(employee3, 50L);

        //THEN
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(company.getBalance()).isEqualTo(5000L);
            softly.assertThat(employee3.getMealDeposits()).isEmpty();
            softly.assertThat(result).isFalse();
        });
    }

    @Test
    void whenCompanyEmitsMealDepositWithBalanceLowerOfAmountReturnFalse() {
        //GIVEN
        Employee employee = EmployeeBuilder.anEmployee().withFirstName("Nicolas").withLastName("MARTIN").build();
        Company company = CompanyBuilder.aCompany().withName("Wedoogift").withBalance(500L).withEmployees(employee).build();

        //WHEN
        boolean result = company.sendMealDeposit(employee, 1000L);

        //THEN
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result).isFalse();
        });
    }
}